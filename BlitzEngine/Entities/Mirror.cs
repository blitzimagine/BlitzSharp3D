﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Mirror:Entity
	{
		internal Mirror(IntPtr pointer):base(pointer){}
		public Mirror(Entity parent=null):base(Blitz3D.CreateMirror_internal(parent)){}
	}
	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]internal static extern IntPtr CreateMirror_internal(IntPtr parent);
	}
}
