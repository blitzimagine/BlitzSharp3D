﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Listener:Entity
	{
		internal Listener(IntPtr pointer):base(pointer){}
		public Listener(Entity parent, float rolloffFactor = 1, float dopplerScale = 1, float distanceScale = 1):base(Blitz3D.CreateListener_internal(parent,rolloffFactor,dopplerScale,distanceScale)){}
	}

	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		internal static extern IntPtr CreateListener_internal(IntPtr parent, float rolloffFactor, float dopplerScale, float distanceScale);
	}
}
