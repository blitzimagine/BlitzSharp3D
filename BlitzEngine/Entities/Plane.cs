﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Plane:Model
	{
		internal Plane(IntPtr pointer):base(pointer){}
		public Plane(int segments=1,Entity parent=null):base(Blitz3D.CreatePlane(segments,parent)){}
	}
	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		private static extern IntPtr CreatePlane_internal(int segments, IntPtr parent);

		public static Entity CreatePlane(int segments = 1, Entity parent = null)
		{
			Entity ret = new Entity(CreatePlane_internal(segments, parent));
			return ret;
		}
	}
}
