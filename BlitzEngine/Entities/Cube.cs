﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Cube:Mesh
	{
		internal Cube(IntPtr pointer):base(pointer){}
		public Cube(Entity parent=null):base(pointer:Blitz3D.CreateCube(parent)){}
	}
	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		private static extern IntPtr CreateCube_internal(IntPtr parent);

		public static Entity CreateCube(Entity parent = null)
		{
			return new Entity(CreateCube_internal(parent));
		}
	}
}
