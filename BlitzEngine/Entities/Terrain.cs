﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Terrain:Model
	{
		internal Terrain(IntPtr pointer):base(pointer){}
		public Terrain(int gridSize,Entity parent=null):base(Blitz3D.CreateTerrain(gridSize,parent)){}
		public Terrain(string heightmapFile, Entity parent = null):base(Blitz3D.LoadTerrain(heightmapFile,parent)){}
	}
	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		private static extern IntPtr CreateTerrain_internal(int gridSize, IntPtr parent);

		public static Entity CreateTerrain(int gridSize, Entity parent = null)
		{
			Entity ret = new Entity(CreateTerrain_internal(gridSize, parent));
			return ret;
		}

		[DllImport(B3DDllLink)]
		private static extern IntPtr LoadTerrain_internal(string heightmapFile, IntPtr parent);

		public static Entity LoadTerrain(string heightmapFile, Entity parent = null)
		{
			Entity ret = new Entity(LoadTerrain_internal(heightmapFile, parent));
			return ret;
		}

		[DllImport(B3DDllLink)]
		private static extern void TerrainDetail_internal(IntPtr terrain, int detailLevel, bool morph);

		public static void TerrainDetail(Entity terrain, int detailLevel, bool morph = false)
		{
			TerrainDetail_internal(terrain.Pointer, detailLevel, morph);
		}

		[DllImport(B3DDllLink)]
		private static extern void TerrainShading_internal(IntPtr terrain, bool enable);

		public static void TerrainShading(Entity terrain, bool enable)
		{
			TerrainShading_internal(terrain.Pointer, enable);
		}

		[DllImport(B3DDllLink)]
		private static extern float TerrainX_internal(IntPtr terrain, float worldX, float worldY, float worldZ);

		public static float TerrainX(Entity terrain, float worldX, float worldY, float worldZ)
		{
			return TerrainX_internal(terrain.Pointer, worldX, worldY, worldZ);
		}

		[DllImport(B3DDllLink)]
		private static extern float TerrainY_internal(IntPtr terrain, float worldX, float worldY, float worldZ);

		public static float TerrainY(Entity terrain, float worldX, float worldY, float worldZ)
		{
			return TerrainY_internal(terrain.Pointer, worldX, worldY, worldZ);
		}

		[DllImport(B3DDllLink)]
		private static extern float TerrainZ_internal(IntPtr terrain, float worldX, float worldY, float worldZ);

		public static float TerrainZ(Entity terrain, float worldX, float worldY, float worldZ)
		{
			return TerrainZ_internal(terrain.Pointer, worldX, worldY, worldZ);
		}

		[DllImport(B3DDllLink)]
		private static extern int TerrainSize_internal(IntPtr terrain);

		public static int TerrainSize(Entity terrain)
		{
			return TerrainSize_internal(terrain.Pointer);
		}

		[DllImport(B3DDllLink)]
		private static extern float TerrainHeight_internal(IntPtr terrain, int terrainX, int terrainZ);

		public static float TerrainHeight(Entity terrain, int terrainX, int terrainZ)
		{
			return TerrainHeight_internal(terrain.Pointer, terrainX, terrainZ);
		}

		[DllImport(B3DDllLink)]
		private static extern void ModifyTerrain_internal(IntPtr terrain, int terrainX, int terrainZ, float height, bool realtime);

		public static void ModifyTerrain(Entity terrain, int terrainX, int terrainZ, float height, bool realtime = false)
		{
			ModifyTerrain_internal(terrain.Pointer, terrainX, terrainZ, height, realtime);
		}
	}
}
