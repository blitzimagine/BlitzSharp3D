﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class BSP:Model
	{
		internal BSP(IntPtr pointer):base(pointer){}

		public BSP(string file, float gammaAdjust = 0.0f, Entity parent = null):base(Blitz3D.LoadBSP(file,gammaAdjust,parent)){}
	}
	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		private static extern IntPtr LoadBSP_internal(string file, float gammaAdjust, IntPtr parent);

		public static Entity LoadBSP(string file, float gammaAdjust = 0.0f, Entity parent = null)
		{
			Entity ret = new Entity(LoadBSP_internal(file, gammaAdjust, parent));
			return ret;
		}

		[DllImport(B3DDllLink)]
		private static extern void BspLighting_internal(IntPtr bsp, bool useLightmaps);

		public static void BspLighting(Entity bsp, bool useLightmaps)
		{
			BspLighting_internal(bsp.Pointer, useLightmaps);
		}

		[DllImport(B3DDllLink)]
		private static extern void BspAmbientLight_internal(IntPtr bsp, float red, float green, float blue);

		public static void BspAmbientLight(Entity bsp, float red, float green, float blue)
		{
			BspAmbientLight_internal(bsp.Pointer, red, green, blue);
		}
	}
}
