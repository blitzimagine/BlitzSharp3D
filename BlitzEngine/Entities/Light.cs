﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Light:Entity
	{
		internal Light(IntPtr pointer):base(pointer){}
		public enum Type
		{
			Directional=1,
			Point=2,
			Spot=3
		}
		
		public Light(Type type=Type.Directional,Entity parent=null):base(Blitz3D.CreateLight((int)type,parent)){}
		
		public void Color(float red, float green, float blue)=>Blitz3D.LightColor(this,red,green,blue);
	}
	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		private static extern IntPtr CreateLight_internal(int type, IntPtr parent);

		public static Entity CreateLight(int type = 1, Entity parent = null)
		{
			Entity ret = new Entity(CreateLight_internal(type, parent));
			return ret;
		}

		[DllImport(B3DDllLink)]
		private static extern void LightColor_internal(IntPtr light, float red, float green, float blue);

		public static void LightColor(Entity light, float red, float green, float blue)
		{
			LightColor_internal(light.Pointer, red, green, blue);
		}

		[DllImport(B3DDllLink)]
		private static extern void LightRange_internal(IntPtr light, float range);

		public static void LightRange(Entity light, float range)
		{
			LightRange_internal(light.Pointer, range);
		}

		[DllImport(B3DDllLink)]
		private static extern void LightConeAngles_internal(IntPtr light, float innerAngle, float outerAngle);

		public static void LightConeAngles(Entity light, float innerAngle, float outerAngle)
		{
			LightConeAngles_internal(light.Pointer, innerAngle, outerAngle);
		}
	}
}
