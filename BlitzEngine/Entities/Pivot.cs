﻿using System;
using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public class Pivot:Entity
	{
		internal Pivot(IntPtr pointer):base(pointer){}
		public Pivot(Entity parent):base(Blitz3D.CreatePivot(parent)){}
	}

	public static partial class Blitz3D
	{
		[DllImport(B3DDllLink)]
		private static extern IntPtr CreatePivot_internal(IntPtr parent);

		public static Entity CreatePivot(Entity parent = null)
		{
			Entity ret = new Entity(CreatePivot_internal(parent));
			return ret;
		}
	}
}
