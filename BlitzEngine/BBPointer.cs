﻿using System;

namespace BlitzEngine
{
	public class BBPointer
	{
		public IntPtr Pointer;

		public BBPointer(IntPtr pointer)
		{
			Pointer = pointer;
		}

		public override int GetHashCode()=>Pointer.GetHashCode();
		public override bool Equals(object obj)=>(obj is BBPointer that) && (this.Pointer==that.Pointer);

		public static bool operator ==(BBPointer a, BBPointer b)
		{
			if (a is null && b is null)
				return true;

			if (a is null && b.Pointer == IntPtr.Zero)
				return true;

			if (b is null && a.Pointer == IntPtr.Zero)
				return true;

			if (a is null || b is null)
				return false;

			return a.Pointer == b.Pointer;
		}

		public static bool operator !=(BBPointer a, BBPointer b)
		{
			return !(a == b);
		}

		//Cast BBPointer to IntPtr, if the BBPointer is null, it will be converted to IntPtr.Zero.
		public static implicit operator IntPtr(BBPointer that)=>that?.Pointer??IntPtr.Zero; //If that is null, use IntPtr.Zero, otherwise use Pointer field.
	}
}
