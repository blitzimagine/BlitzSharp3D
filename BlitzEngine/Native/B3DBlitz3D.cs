﻿using System.Runtime.InteropServices;

namespace BlitzEngine
{
	public static partial class Blitz3D
	{
		// -------
		// Blitz3D
		// -------

		[DllImport(B3DDllLink)]
		public static extern void LoaderMatrix(string fileExt, float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz);

		[DllImport(B3DDllLink)]
		public static extern void HwMultiTex(bool enable);

		[DllImport(B3DDllLink)]
		public static extern int HwTexUnits();

		[DllImport(B3DDllLink)]
		public static extern int GfxDriverCaps3D();

		[DllImport(B3DDllLink)]
		public static extern void WBuffer(bool enable);

		[DllImport(B3DDllLink)]
		public static extern void Dither(bool enable);

		[DllImport(B3DDllLink)]
		public static extern void AntiAlias(bool enable);

		[DllImport(B3DDllLink)]
		public static extern void WireFrame(bool enable);

		[DllImport(B3DDllLink)]
		public static extern void AmbientLight(float red, float green, float blue);

		[DllImport(B3DDllLink)]
		public static extern void ClearCollisions();

		[DllImport(B3DDllLink)]
		public static extern void Collisions(int srcType, int destType, int method, int response);

		[DllImport(B3DDllLink)]
		public static extern void UpdateWorld(float elapsed = 1.0f);

		[DllImport(B3DDllLink)]
		public static extern void CaptureWorld();

		[DllImport(B3DDllLink)]
		public static extern void RenderWorld(float tween = 1.0f);

		[DllImport(B3DDllLink)]
		public static extern void ClearWorld(bool entities = true, bool brushes = true, bool textures = true);

		[DllImport(B3DDllLink)]
		public static extern int ActiveTextures();

		[DllImport(B3DDllLink)]
		public static extern int TrisRendered();

		[DllImport(B3DDllLink)]
		public static extern float Stats3D(int type);

		

		[DllImport(B3DDllLink)]
		public static extern void ClearTextureFilters();

		[DllImport(B3DDllLink)]
		public static extern void TextureFilter(string matchText, int textureFlags = 0);
	}
}
